Name:             strongswan
Version:          5.7.2
Release:          4
Summary:          An OpenSource IPsec-based VPN and TNC solution
License:          GPLv2+
URL:              http://www.strongswan.org/
Source0:          http://download.strongswan.org/strongswan-%{version}.tar.bz2
BuildRequires:    gcc systemd-devel gmp-devel libcurl-devel NetworkManager-libnm-devel openldap-devel
BuildRequires:    openssl-devel sqlite-devel gettext-devel trousers-devel libxml2-devel pam-devel
BuildRequires:    json-c-devel libgcrypt-devel systemd-devel iptables-devel
Requires(post):   systemd
Requires(preun):  systemd
Requires(postun): systemd
Provides:         strongswan-libipsec = %{version}-%{release}
Provides:         strongswan-charon-nm = %{version}-%{release}
Provides:         strongswan-sqlite = %{version}-%{release}
Provides:         strongswan-tnc-imcvs = %{version}-%{release}
Provides:         strongswan-libipsec%{?_isa} = %{version}-%{release}
Provides:         strongswan-charon-nm%{?_isa} = %{version}-%{release}
Provides:         strongswan-sqlite%{?_isa} = %{version}-%{release}
Provides:         strongswan-tnc-imcvs%{?_isa} = %{version}-%{release}
Obsoletes:        strongswan-libipsec < %{version}-%{release}
Obsoletes:        strongswan-charon-nm < %{version}-%{release}
Obsoletes:        strongswan-sqlite < %{version}-%{release}
Obsoletes:        strongswan-tnc-imcvs < %{version}-%{release}
Obsoletes:        %{name}-NetworkManager < 0:5.0.4-5
Conflicts:        %{name}-NetworkManager < 0:5.0.4-5
Conflicts:        NetworkManager-strongswan < 1.4.2-1

%description
The strongSwan IPsec implementation supports both the IKEv1 and IKEv2 key exchange
protocols in conjunction with the native NETKEY IPsec stack of the Linux kernel.

%package_help

%prep
%autosetup -n %{name}-%{version}

%build
%configure --disable-static --with-ipsec-script=strongswan --sysconfdir=%{_sysconfdir}/strongswan \
           --with-ipsecdir=%{_libexecdir}/strongswan --bindir=%{_libexecdir}/strongswan \
           --with-ipseclibdir=%{_libdir}/strongswan --with-fips-mode=2 --enable-bypass-lan \
           --enable-tss-trousers --enable-nm --enable-systemd --enable-openssl --enable-unity \
           --enable-ctr --enable-ccm --enable-gcm --enable-chapoly --enable-md4 --enable-gcrypt \
           --enable-newhope --enable-xauth-eap --enable-xauth-pam --enable-xauth-noauth \
           --enable-eap-identity --enable-eap-md5 --enable-eap-gtc --enable-eap-tls --enable-eap-ttls \
           --enable-eap-peap --enable-eap-mschapv2 --enable-eap-tnc --enable-eap-sim --enable-eap-sim-file \
           --enable-eap-aka --enable-eap-aka-3gpp --enable-eap-aka-3gpp2 --enable-eap-dynamic --enable-eap-radius \
           --enable-ext-auth --enable-ipseckey --enable-pkcs11 --enable-tpm --enable-farp --enable-dhcp \
           --enable-ha --enable-led --enable-sql --enable-sqlite --enable-tnc-ifmap --enable-tnc-pdp \
           --enable-tnc-imc --enable-tnc-imv --enable-tnccs-20 --enable-tnccs-11 --enable-tnccs-dynamic \
           --enable-imc-test --enable-imv-test --enable-imc-scanner --enable-imv-scanner --enable-imc-attestation \
           --enable-imv-attestation --enable-imv-os --enable-imc-os --enable-imc-swid --enable-imv-swid \
           --enable-imc-swima --enable-imv-swima --enable-imc-hcd --enable-imv-hcd --enable-curl \
           --enable-cmd --enable-acert --enable-aikgen --enable-vici --enable-swanctl --enable-duplicheck \
           --enable-kernel-libipsec \
%ifarch x86_64 %{ix86}
           --enable-aesni
%endif

for p in bypass-lan; do
    echo -e "\ncharon.plugins.${p}.load := no" >> conf/plugins/${p}.opt
done

make %{?_smp_mflags}

%install
%make_install

mv %{buildroot}%{_sysconfdir}/strongswan/dbus-1 %{buildroot}%{_sysconfdir}/
rm -rf %{buildroot}%{_libdir}/strongswan/*.so

chmod 644 %{buildroot}%{_sysconfdir}/strongswan/strongswan.conf
install -d -m 700 %{buildroot}%{_sysconfdir}/strongswan/ipsec.d
install -d -m 700 %{buildroot}%{_sysconfdir}/strongswan/ipsec.d/{aacerts acerts cacerts certs crls ocspcerts private reqs}

%delete_la

%preun
%systemd_preun strongswan.service

%post
%systemd_post strongswan.service

%postun
%systemd_postun_with_restart strongswan.service

%files
%defattr(-,root,root)
%doc README ChangeLog TODO NEWS
%license COPYING
%dir %attr(0700,root,root) %{_sysconfdir}/strongswan
%config(noreplace) %{_sysconfdir}/strongswan/*
%dir %{_libdir}/strongswan
%dir %{_libdir}/strongswan/plugins
%dir %{_libdir}/strongswan/imcvs
%dir %{_libexecdir}/strongswan
%{_unitdir}/*.service
%{_sbindir}/*
%{_libdir}/strongswan/*.so.*
%{_libdir}/strongswan/plugins/*.so
%{_libdir}/strongswan/imcvs/*.so
%{_libexecdir}/strongswan/*
%{_datadir}/strongswan/templates/config/
%{_datadir}/strongswan/templates/database/
%dir %{_datadir}/strongswan/swidtag
%{_datadir}/strongswan/swidtag/*.swidtag
%{_sysconfdir}/dbus-1/system.d/nm-strongswan-service.conf

%files help
%{_mandir}/man1/*1.gz
%{_mandir}/man5/*5.gz
%{_mandir}/man8/*8.gz

%changelog
* Fri Feb 14 2020 Ling Yang <lingyang2@huawei.com> - 5.7.2-4
- Package init
